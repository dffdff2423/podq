# a "fake" Makefile for podq

all:
	chicken-install -n
.PHONY: all

install:
	chicken-install
.PHONY: install

uninstall:
	chicken-uninstall podq
.PHONY: uninstall

clean:
	rm -f *.o *.so *.build.sh *.install.sh *.link *.import.scm podq-runner
.PHONY: clean
