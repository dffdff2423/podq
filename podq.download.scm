(import (r7rs))
(declare (module (podq download)))

(import (scheme base)
        (scheme case-lambda)
        (scheme file)
        (scheme read)
        (scheme write))

(import (chicken pretty-print))

(import (podq util))

(import (http-client)
        (rss))

(export (sync-episodes))

(define (download-file url output-path)
  (with-output-to-file output-path
    (lambda ()
      (display (with-input-from-request url
                                        #f
                                        read-string)))))

(define (get-xml-path-for-podcast podcast-name)
 (string-append +xdg-data-dir+
                "/"
                podcast-name
                ".xml"))

(define (get-episode-alist-path-for-podcast podcast-name)
 (string-append +xdg-data-dir+
                "/"
                podcast-name
                ".alist"))

(define (download-files alist)
  (for-each
   (lambda (podcast)
     (let ((file-path (get-xml-path-for-podcast (car podcast))))
       (display (car podcast))
       (display ": ")
       (display (cdr podcast))
       (display " > ")
       (display file-path)
       (newline)
       (download-file (cdr podcast)
                      file-path)))
   alist))

(define (get-urls-from-rss-enclosures enclosures)
  (map (lambda (enc)
         (cons (rss:enclosure-url enc)
               (rss:enclosure-type enc)))
       enclosures))

(define (rss-file->episode-alist-file rss-path alist-path)
  (parameterize ((current-input-port (open-input-file rss-path))
                 (current-output-port (open-output-file alist-path)))
    (let* ((feed (rss:read))
           (items (rss:feed-items feed)))
      (pp (map (lambda (item)
                (cons (rss:item-title item)
                      (get-urls-from-rss-enclosures
                       (rss:item-enclosures item))))
               items)))))

(define (extract-episodes alist)
  (for-each
   (lambda (podcast)
     (let ((rss-path (get-xml-path-for-podcast (car podcast)))
           (alist-path (get-episode-alist-path-for-podcast
                        (car podcast))))
       (display (car podcast))
       (display ": ")
       (display rss-path)
       (display " > ")
       (display alist-path)
       (newline)
       (rss-file->episode-alist-file rss-path alist-path)))
   alist))

(define (sync-episodes alist)
  (display "downloading rss feeds ...")
  (newline)
  (download-files alist)
  (newline)
  (newline)
  (display "extracting episodes ...")
  (newline)
  (extract-episodes alist))

(define list-avalible
  (lambda () '()))
  ;(case-lambda
  ;  (() ())
  ;  ((podcast) ()))
