(import (scheme base)
        (scheme process-context)
        (scheme write)
        (podq))


(main (command-line))

#;
(with-exception-handler
  (lambda (exep)
    (parameterize ((current-output-port (current-error-port)))
      (display "Error: ")
      (if (error-object? exep)
         (display (error-object-message exep))
         (dispay exep))
      (newline)
      (exit 1)))
  (lambda ()
    (main (command-line))))
