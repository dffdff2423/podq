(declare (module (podq util)))

(import (r7rs))

(import (scheme base)
        (scheme cxr)
        (scheme file)
        (scheme process-context)
        (scheme read))

(import (chicken file)
        (chicken irregex)
        (chicken pathname))

(export (get-config-value)
        (when-not-in-repl)
;        (+xdg-cache-dir+)
        (+xdg-config-dir+)
        (+xdg-data-dir+))


(define-syntax when-not-in-repl
  (syntax-rules ()
    ((_ . body) (cond-expand
                  (compiling . body)
                  (else)))))

(define (get-xdg-dir env def)
  (let ((env (get-environment-variable env)))
    (if env
        (create-directory (string-append env "/podq"))
        (create-directory (string-append (get-environment-variable "HOME")
                                         "/"
                                         def
                                         "/podq")))))

;(define +xdg-cache-dir+ (get-xdg-dir "XDG_CACHE_HOME" ".cache"))
(define +xdg-config-dir+ (get-xdg-dir "XDG_CONFIG_HOME" ".config"))
(define +xdg-data-dir+ (get-xdg-dir "XDG_DATA_HOME" ".local/share"))

(define (get-config-value key conf)
  (let ((val (alist-ref key conf)))
    (if (not val)
      (raise (error (string-append "cannot find key `"
                                   (symbol->string key)
                                   "` in configuration file")))
      val)))

(define (get-podcast-ident ep name)
  (string-append name
                 "/"
                 (car ep)
                 "."
                 (pathname-extension (caadr ep))))

; TODO: support multiple enclosures
(define (write-podcast-epidsode-identifier name episode-alist)
  (for-each (lambda (ep)
              (get-podcast-ident ep name)
              (newline))
            episode-alist))

; TODO: support multiple enclosures
;(define (get-url-for-podcast-ident))
