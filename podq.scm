(import (r7rs))
(declare (module (podq)))

(import (scheme base)
        (scheme process-context)
        (scheme write))

(import (chicken pretty-print))


(import (matchable)
        (prefix (optimism getopt-long) "optimism:"))

(import (podq download)
        (podq util))

(export (main))

(define-constant +podq-version+ "0.1.0")

(define-constant +opts+
  '(((-A --all-available))
    ((-a --available) . podcast)
    ((-c,--podcasts))
    ((-d --download) . episode)
    ((-h --help))
    ((-l --list-downloaded))
    ((-p --play) . episode)
    ((-s --stop))
    ((-V --version))
    ((-y --sync))))

(define (usage argv-1)
  (display (string-append
            "Usage: " argv-1 " [OPTIONS ...]\n"
            "downloads and plays podcasts using mpd\n"
            "\n"
            "Options: \n"
            "\t-A,--all-available          lists all available episodes"
            "\t-a,--available <podcast>   lists available episodes, from <podcast>\n"
            "\t-c,--podcasts  \t\t lists the names of podcasts in the config file"
            "\t-d,--download <episode>    download <episode>\n"
            "\t-h,--help  \t\t show this message then exit with code 1\n"
            "\t-l,--list-downloaded  \t lists downloaded podcasts\n"
            "\t-p,--play <episode>  \t play <episode> via mpd, possibly restoring past progress\n"
            "\t-s,--stop  \t\t stops the current mpd \"song\" and saves it's progress to a file\n"
            "\t-V,--version  \t\t show version then exit with code 1\n"
            "\t-y,--sync  \t\t download available episodes")
           (current-error-port)))

(define (show-version)
  (display (string-append "podq\t" +podq-version+ "\n")
           (current-error-port)))


(define (read-configuration)
  (with-input-from-file (string-append +xdg-config-dir+
                                       "/config.alist")
   read))

(define *configuration* (read-configuration))

(define process-remaining-opts
  (match-lambda
    [(or `((-a . podcast) . ,rest)
         `((--avalable . podcast) . ,rest))]
    [(or `((-A) . ,rest)
         `((--all-avalable) . ,rest))]
    [(or `((-c) . ,rest)
         `((--podcasts . ,rest)))
     (for-each (lambda (p)
                 (display (car p))
                 (newline))
      (get-config-value 'podcasts
                        *configuration*))
     (process-remaining-opts rest)]
    [(or `((-y) . ,rest)
         `((--sync) . ,rest))
     (sync-episodes (get-config-value 'podcasts *configuration*))
     (process-remaining-opts rest)]

    [r (display r
                (current-error-port)) (newline)]))

(define (process-opts opt-lst)
  (let ((remaining-opts '()))
    (for-each (lambda (opt)
                (cond ((or (equal? '(-h) opt)
                           (equal? '(--help) opt))
                       (usage (car (command-line)))
                       (when-not-in-repl (exit 1)))
                      ((or (equal? '(-V) opt)
                           (equal? '(--version) opt))
                       (show-version)
                       (when-not-in-repl (exit 1)))
                      (else (set! remaining-opts (cons opt remaining-opts)))))
              opt-lst)
    (process-remaining-opts (reverse remaining-opts))))

; TODO: better error messages
(define (main argv)
  (process-opts (optimism:parse-command-line (cdr argv)
                                             +opts+)))
